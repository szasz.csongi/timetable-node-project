import { findOwnerOfPdf } from '../db/apiDb.js';

// Check if the logged in user is indeed the owner of the pdf
export default async function checkPdfsOwner(req, res, next) {
    const PdfId = req.params.id;

    try {
        const teacherUsername = await findOwnerOfPdf(PdfId);
        if (teacherUsername.teacherUsername !== res.locals.payload.username) {
            return res.status(403).send('You are not the owner of this pdf');
        }
    } catch (err) {
        res.write('Error in finding subject by Id', err);
        res.status(500);
        return res.end();
    }

    return next();
}
