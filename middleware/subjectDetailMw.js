import { findSubjectById } from '../db/newSubjectDb.js';

// Check the correct format of the file
export function checkPdfUpload(req, res, next) {
    const fileHandler = req.files.pdfForSubject;
    if (fileHandler.type !== 'application/pdf') {
        return res.status(406).send('The file is not a pdf');
    }

    return next();
}

// Check if there is a subject with this id
export async function checkSubjectIfExists(req, res, next) {
    try {
        const exists = await findSubjectById(req.fields.subjectid);
        if (typeof exists === 'undefined') {
            return res.status(404).send('There is no subject with this Id');
        }
    } catch (err) {
        res.write('Error in finding subject by Id', err);
        res.status(500);
        return res.end();
    }

    return next();
}

// Check if this user is indeed the owner of this subject
export async function checkTeacherOwner(req, res, next) {
    try {
        const subject = await findSubjectById(req.fields.subjectid);
        if (subject.teacherUsername !== res.locals.payload.username) {
            return res.status(403).send('You are not the owner of this subject');
        }
    } catch (err) {
        res.write('Error in finding subject by Id', err);
        res.status(500);
        return res.end();
    }

    return next();
}
