import joi from 'joi';
import { selectUserRequestNumberByUsername } from '../db/newRequestDb.js';
import { findSubjectByTeacher } from '../db/subjectsDb.js';

// Check if form is completed correctly before psot
export async function checkRequest(req, res, next) {
    const schema = joi.object().keys({
        appropriate: joi.number().integer().min(0).max(1)
            .required(),
        day: joi.number().integer().min(1).max(5)
            .required(),
        hour: joi.number().integer().multiple(2).min(8)
            .max(18)
            .required(),
        till: joi.number().integer().multiple(2).min(10)
            .max(20)
            .required(),
    }).unknown(true);

    const { error } = schema.validate(req.body);
    const valid = error == null;

    if (!valid) {
        const { details } = error;
        const message = details.map((i) => i.message).join('\n');

        return res.status(400).json({ error: message });
    }

    if (parseInt(req.body.hour, 10) >= parseInt(req.body.till, 10)) {
        return res.status(400).send('The ending of subject should be greater than the beginning');
    }

    try {
        const subjects = await findSubjectByTeacher(res.locals.payload.username);
        let found = false;
        for (let i = 0; i < subjects.length; i += 1) {
            if (req.body.subjectid === subjects[i].subjectid) {
                found = true;
                break;
            }
        }
        if (!found) {
            return res.status(403).send('This is not your subject');
        }
    } catch (err) {
        res.write('Error in finding subject by Teacher username', err);
        res.status(500);
        return res.end();
    }

    return next();
}

// Check if user has enough requests left to make a new one
export async function checkNrRequests(req, res, next) {
    try {
        const requests = await selectUserRequestNumberByUsername(res.locals.payload.username);
        if (requests <= 0) {
            return res.status(400).send('You have no more requests');
        }
    } catch (err) {
        res.write('Error in findig the number of requests of the logged in user', err);
        res.status(500);
        return res.end();
    }

    return next();
}
