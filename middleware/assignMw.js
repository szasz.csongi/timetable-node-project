import { findSubjectById, findUserById } from '../db/newSubjectDb.js';

export function checkIds(req, res, next) {
    if (!req.body.subjectid) {
        return res.status(400).send('The subject Id is empty');
    }

    if (!req.body.studentid) {
        return res.status(400).send('The student Id is empty');
    }

    return next();
}

export async function checkSubjectIfExists(req, res, next) {
    try {
        const exists = await findSubjectById(req.body.subjectid);
        if (typeof exists === 'undefined') {
            return res.status(400).send('There is no subject with this Id');
        }
    } catch (err) {
        res.write('Error in finding subject by Id fdd', err);
        res.status(500);
        return res.end();
    }

    return next();
}

export async function checkUserIfExists(req, res, next) {
    try {
        const exists = await findUserById(req.body.studentid);
        if (typeof exists === 'undefined') {
            return res.status(400).send('There is no user with this Id');
        }
    } catch (err) {
        res.write('Error in finding user by Id fdd', err);
        res.status(500);
        return res.end();
    }

    return next();
}
