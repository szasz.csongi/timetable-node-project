import joi from 'joi';

import { findSubjectById } from '../db/newSubjectDb.js';

// Check if the user is admin or is the owner of this subject
export async function checkTeacherOwnerOrAdmin(req, res, next) {
    try {
        const subject = await findSubjectById(req.query.subjectId);
        if (subject.teacherUsername !== res.locals.payload.username
            && res.locals.payload.userRole === 0) {
            return res.status(400).send('You are not the owner of this subject or you are not an admin');
        }
    } catch (err) {
        res.write('Error in finding subject by Id', err);
        res.status(500);
        return res.end();
    }

    return next();
}

// Check if the form is completed correctly before post
export async function checkEditSubject(req, res, next) {
    const schema = joi.object().keys({
        subjectid: joi.string().required(),
        subjectname: joi.string().required(),
        grade: joi.number().integer().min(1).max(6)
            .required(),
        course: joi.number().integer().min(1).max(14)
            .required(),
        seminar: joi.number().integer().min(1).max(14)
            .required(),
        labor: joi.number().integer().min(1).max(14)
            .required(),
    }).unknown(true);

    const { error } = schema.validate(req.body);
    const valid = error == null;

    if (!valid) {
        const { details } = error;
        const message = details.map((i) => i.message).join('\n');

        return res.status(400).json({ error: message });
    }

    try {
        const exists = await findSubjectById(req.body.subjectid);
        if (exists) {
            if (req.query.subjectId !== req.body.subjectid) {
                return res.status(400).send('This subject allready exists with this ID');
            }
        }
    } catch (err) {
        res.write('Error in finding subject by Id', err);
        res.status(500);
        return res.end();
    }

    return next();
}
