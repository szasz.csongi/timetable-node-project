import joi from 'joi';

// Checki if the accept request form is comleted correctly
export default async function checkAcceptRequest(req, res, next) {
    const schema = joi.object().keys({
        day: joi.number().integer().min(1).max(5)
            .required(),
        hour: joi.number().integer().multiple(2).min(8)
            .max(18)
            .required(),
    }).unknown(true);

    const { error } = schema.validate(req.body);
    const valid = error == null;

    if (!valid) {
        const { details } = error;
        const message = details.map((i) => i.message).join('\n');

        return res.status(400).json({ error: message });
    }

    return next();
}
