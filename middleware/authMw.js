import jwt from 'jsonwebtoken';
import joi from 'joi';

import { checkIfExistsUser, selectUserByUsername } from '../db/authDb.js';
import secret from '../util/config.js';

export function decodeJWTToken(req, res, next) {
    res.locals.payload = {};
    if (req.cookies.token) {
        try {
            res.locals.payload = jwt.verify(req.cookies.token, secret);
        } catch (e) {
            res.clearCookie('token');
        }
    }
    next();
}

// Checking if user is logged in
export function checkJWTToken(req, res, next) {
    if (!(res.locals.payload && Object.keys(res.locals.payload).length === 0
    && res.locals.payload.constructor === Object
    )) {
        return next();
    }
    return res.redirect('/auth/login');
}

// Checking if person is admin or not
export async function checkIfAdmin(req, res, next) {
    const user = await selectUserByUsername(res.locals.payload.username);
    if (user.userRole === 0) {
        return res.status(403).send('Your are not admin');
    }
    return next();
}

// Checking if registration form was completed correctly
export async function checkRegistration(req, res, next) {
    const schema = joi.object().keys({
        registrationFirstName: joi.string().required(),
        registrationLastName: joi.string().required(),
        registrationUsername: joi.string().required(),
        password: joi.string().required(),
        confirmPassword: joi.string().required(),
        email: joi.string().required(),
        age: joi.number().required(),
        telephone: joi.string().required(),
    }).unknown(true);

    const { error } = schema.validate(req.body);
    const valid = error == null;

    if (!valid) {
        const { details } = error;
        const message = details.map((i) => i.message).join('\n');

        return res.status(400).json({ error: message });
    }

    try {
        const mistake = await checkIfExistsUser(req.body.registrationUsername);
        if (mistake.Counter !== 0) {
            return res.status(400).send('There is allready a user using this username!');
        }
    } catch (err) {
        res.write('Registration error: ', err);
        res.status(500);
        res.end();
    }

    return next();
}

// Checking if login form was completed correctly
export async function checkLogin(req, res, next) {
    if (!req.body.loginUsername) {
        return res.status(400).send('Username is empty');
    }

    if (!req.body.password) {
        return res.status(400).send('Password is empty');
    }

    try {
        const person = await selectUserByUsername(req.body.loginUsername);
        if (typeof person === 'undefined') {
            return res.status(401).send('There is no such username, you should registrate');
        }
        if (person.confirmed === 0) {
            return res.status(403).send('Your account is not confirmed by an admin');
        }
    } catch (err) {
        res.write('Login error: ', err);
        res.status(500);
        res.end();
    }

    return next();
}
