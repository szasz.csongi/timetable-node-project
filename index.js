import express from 'express';
import path from 'path';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import { collectDefaultMetrics, register } from 'prom-client'; // Import Prometheus libraries
import {client} from "./elasticsearch/ellasticConnection.js";

import subjectRoutes from './routes/subjectsRoutes.js';
import newSubjectRoutes from './routes/newSubjectRoutes.js';
import editSubjectRoutes from './routes/editSubjectRoutes.js';
import subjectDeatilRoutes from './routes/subjectDetailRoutes.js';
import assignRoutes from './routes/assignRoutes.js';
import apiRoutes from './routes/apiRoutes.js';
import authRoutes from './routes/authRoutes.js';
import usersRoutes from './routes/usersRoutes.js';
import newRequestRoutes from './routes/newRequestRoutes.js';
import manageRequestsRoutes from './routes/manageRequestsRoutes.js';
import acceptRequestRoutes from './routes/acceptRequestRoutes.js';
import timetableRoutes from './routes/timetableRoutes.js';

import { decodeJWTToken } from './middleware/authMw.js';

collectDefaultMetrics();

const app = express();
const staticDir = path.join(process.cwd(), 'static');
const uploadDir = path.join(process.cwd(), 'uploadDir');

app.use(morgan('tiny'));
app.use(cookieParser());
app.use(express.static(staticDir));
app.use(express.static(uploadDir));
app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.set('views', path.join(process.cwd(), 'views'));

app.use(decodeJWTToken);

app.use('/newSubject', newSubjectRoutes);
app.use('/editSubject', editSubjectRoutes);
app.use('/subjectDetail', subjectDeatilRoutes);
app.use('/assign', assignRoutes);
app.use('/api', apiRoutes);
app.use('/auth', authRoutes);
app.use('/users', usersRoutes);
app.use('/newRequest', newRequestRoutes);
app.use('/manageRequests', manageRequestsRoutes);
app.use('/acceptRequest', acceptRequestRoutes);
app.use('/timetable', timetableRoutes);
app.use(subjectRoutes);

app.get('/metrics', async (req, res) => {
    try {
        const metrics = await register.metrics();
        res.set('Content-Type', register.contentType);
        res.end(metrics);
    } catch (err) {
        console.error('Error collecting metrics:', err);
        res.status(500).send('Internal Server Error');
    }
});

app.get('/search-title/:title', function (req, res) {
    // Access title like this: req.params.title

    /* Query using slop to allow for unexact matches */
    client.search({
    index: 'search-articles',
    type: 'articles',
    body: {
      "query": {   
        "match_phrase": { 
          "Title": { query: req.params.title, slop: 100 }
        } 
      } 
    }
    
    }).then(function(resp) {
        console.log("Successful query! Here is the response:", resp);
        res.send(resp);
    }, function(err) {
        console.trace(err.message);
        res.send(err.message);
    });
  });

app.listen(8080, () => {
    console.log('Server listening on http://localhost:8080/');
});
