// Delete a timetable, call the api and disapear the element immediately
async function deleteFromTimetable(event, timetableId) {
    const response = await fetch(`/api/deleteTimetable/${timetableId}`, {
        method: 'DELETE',
    });

    if (response.status === 204) {
        const parent = event.target.parentNode.parentNode;
        parent.remove();
    } else {
        const body = await response.json();
        const printError = document.getElementById(`timetableId${timetableId}`);
        const div = document.createElement('div');
        div.innerHTML = `${printError.innerHTML} ${body.message}`;
        printError.appendChild(div);
    }
}
