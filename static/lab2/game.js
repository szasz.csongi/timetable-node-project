let nickname = '';
let money = 0;

function startGame() {
    const idUserData = document.getElementById('user-data');
    idUserData.style.display = 'block';

    nickname = document.getElementById('nickname').value;
    money = document.getElementById('money').value;

    document.getElementById('user-nickname').innerHTML = nickname;
    document.getElementById('user-money').innerHTML = money;

    const idContainer = document.getElementById('container');
    idContainer.style.display = 'flex';

    const computerMoney = Math.round(Math.random() * 10) + 1;
    document.getElementById('computer-money').innerHTML = computerMoney;

    const userWon = document.getElementById('user-won');
    const computerWon = document.getElementById('computer-won');

    userWon.style.display = 'none';
    computerWon.style.display = 'none';
}

function rock() {
    const scrissors = document.getElementById('user-scrissors');
    scrissors.style.display = 'none';

    const paper = document.getElementById('user-paper');
    paper.style.display = 'none';

    const rock = document.getElementById('user-rock');
    rock.style.filter = 'grayscale(0%)';

    const button = document.getElementById('button');
    button.style.display = 'block';

    const computer = Math.random();

    if (computer <= 0.33) {
        const computerRock = document.getElementById('computer-rock');
        computerRock.style.display = 'flex';
    } else if (computer <= 0.66) {
        const computerScrissors = document.getElementById('computer-scrissors');
        computerScrissors.style.display = 'flex';

        let computerMoney = document.getElementById('computer-money').textContent;
        computerMoney -= 1;
        document.getElementById('computer-money').innerHTML = computerMoney;
    } else {
        const computerPaper = document.getElementById('computer-paper');
        computerPaper.style.display = 'flex';

        let userMoney = document.getElementById('user-money').textContent;
        userMoney -= 1;
        document.getElementById('user-money').innerHTML = userMoney;
    }
}

function scrissors() {
    const scrissors = document.getElementById('user-scrissors');
    scrissors.style.filter = 'grayscale(0%)';

    const paper = document.getElementById('user-paper');
    paper.style.display = 'none';

    const rock = document.getElementById('user-rock');
    rock.style.display = 'none';

    const button = document.getElementById('button');
    button.style.display = 'block';

    const computer = Math.random();

    if (computer <= 0.33) {
        const computerRock = document.getElementById('computer-rock');
        computerRock.style.display = 'flex';

        let userMoney = document.getElementById('user-money').textContent;
        userMoney -= 1;
        document.getElementById('user-money').innerHTML = userMoney;
    } else if (computer <= 0.66) {
        const computerScrissors = document.getElementById('computer-scrissors');
        computerScrissors.style.display = 'flex';
    } else {
        const computerPaper = document.getElementById('computer-paper');
        computerPaper.style.display = 'flex';

        let computerMoney = document.getElementById('computer-money').textContent;
        computerMoney -= 1;
        document.getElementById('computer-money').innerHTML = computerMoney;
    }
}

function paper() {
    const scrissors = document.getElementById('user-scrissors');
    scrissors.style.display = 'none';

    const paper = document.getElementById('user-paper');
    paper.style.filter = 'grayscale(0%)';

    const rock = document.getElementById('user-rock');
    rock.style.display = 'none';

    const button = document.getElementById('button');
    button.style.display = 'block';

    const computer = Math.random();

    if (computer <= 0.33) {
        const computerRock = document.getElementById('computer-rock');
        computerRock.style.display = 'flex';

        let computerMoney = document.getElementById('computer-money').textContent;
        computerMoney -= 1;
        document.getElementById('computer-money').innerHTML = computerMoney;
    } else if (computer <= 0.66) {
        const computerScrissors = document.getElementById('computer-scrissors');
        computerScrissors.style.display = 'flex';

        let userMoney = document.getElementById('user-money').textContent;
        userMoney -= 1;
        document.getElementById('user-money').innerHTML = userMoney;
    } else {
        const computerPaper = document.getElementById('computer-paper');
        computerPaper.style.display = 'flex';
    }
}

function again() {
    const rock = document.getElementById('user-rock');
    rock.style.display = 'flex';
    rock.style.filter = 'grayscale(100%)';

    const scrissors = document.getElementById('user-scrissors');
    scrissors.style.display = 'flex';
    scrissors.style.filter = 'grayscale(100%)';

    const paper = document.getElementById('user-paper');
    paper.style.display = 'flex';
    paper.style.filter = 'grayscale(100%)';

    const computerRock = document.getElementById('computer-rock');
    computerRock.style.display = 'none';

    const computerScrissors = document.getElementById('computer-scrissors');
    computerScrissors.style.display = 'none';

    const computerPaper = document.getElementById('computer-paper');
    computerPaper.style.display = 'none';

    const button = document.getElementById('button');
    button.style.display = 'none';

    const userMoney = document.getElementById('user-money').textContent;
    const computerMoney = document.getElementById('computer-money').textContent;

    const userWon = document.getElementById('user-won');
    const computerWon = document.getElementById('computer-won');

    if (userMoney == 0) {
        computerWon.style.display = 'block';

        const idContainer = document.getElementById('container');
        idContainer.style.display = 'none';
    }

    if (computerMoney == 0) {
        userWon.style.display = 'block';

        const idContainer = document.getElementById('container');
        idContainer.style.display = 'none';
    }
}
