function checkFirstName() {
    if (!document.forms.formId.firstname.validity.valid) {
        showFirstNameError();
    } else {
        hideFirstNameError();
    }
}

function showFirstNameError() {
    const idFirstNameError = document.getElementById('first-name-error');
    idFirstNameError.style.display = 'block';
    hideSubmit();
}

function hideFirstNameError() {
    const idFirstNameError = document.getElementById('first-name-error');
    idFirstNameError.style.display = 'none';
    if (checkAllValidation()) {
        showSubmit();
    }
}

function checkLastName() {
    if (!document.forms.formId.lastname.validity.valid) {
        showLastNameError();
    } else {
        hideLastNameError();
    }
}

function showLastNameError() {
    const idLastNameError = document.getElementById('last-name-error');
    idLastNameError.style.display = 'block';
    hideSubmit();
}

function hideLastNameError() {
    const idLastNameError = document.getElementById('last-name-error');
    idLastNameError.style.display = 'none';
    if (checkAllValidation()) {
        showSubmit();
    }
}

function checkEmail() {
    const email = document.getElementById('email').value;
    if (!/[a-zA-Z0-9\._-]+@(gmail|yahoo)\.com$/.test(email)) {
        showEmailError();
    } else {
        hideEmailError();
    }
}

function showEmailError() {
    const idEmailError = document.getElementById('email-error');
    idEmailError.style.display = 'block';
    hideSubmit();
}

function hideEmailError() {
    const idEmailError = document.getElementById('email-error');
    idEmailError.style.display = 'none';
    if (checkAllValidation()) {
        showSubmit();
    }
}

function checkUrl() {
    const webpage = document.getElementById('webpage').value;
    if (!/(http|https):\/\/[0-9a-zA-Z_\-\.]+[a-zA-Z/]+$/.test(webpage)) {
        showUrlError();
    } else {
        hideUrlError();
    }
}

function showUrlError() {
    const idUrlError = document.getElementById('web-page-error');
    idUrlError.style.display = 'block';
    hideSubmit();
}

function hideUrlError() {
    const idUrlError = document.getElementById('web-page-error');
    idUrlError.style.display = 'none';
    if (checkAllValidation()) {
        showSubmit();
    }
}

function checkNickname() {
    if (!document.forms.formId.nickname.validity.valid) {
        showNicknameError();
    } else {
        hideNicknameError();
    }
}

function showNicknameError() {
    const idUrlError = document.getElementById('nickname-error');
    idUrlError.style.display = 'block';
    hideSubmit();
}

function hideNicknameError() {
    const idUrlError = document.getElementById('nickname-error');
    idUrlError.style.display = 'none';
    if (checkAllValidation()) {
        showSubmit();
    }
}

function checkMoney() {
    if (!document.forms.formId.money.validity.valid) {
        showMoneyError();
    } else {
        hideMoneyError();
    }
}

function showMoneyError() {
    const idUrlError = document.getElementById('money-error');
    idUrlError.style.display = 'block';
    hideSubmit();
}

function hideMoneyError() {
    const idUrlError = document.getElementById('money-error');
    idUrlError.style.display = 'none';
    if (checkAllValidation()) {
        showSubmit();
    }
}

function hideSubmit() {
    const idSubmitButton = document.getElementById('submit-button');
    idSubmitButton.style.display = 'none';
}

function showSubmit() {
    const idSubmitButton = document.getElementById('submit-button');
    idSubmitButton.style.display = 'block';
}

function checkAllValidation() {
    return  (document.forms.formId.firstname.validity.valid
            && document.forms.formId.lastname.validity.valid
            && document.forms.formId.email.validity.valid
            && document.forms.formId.webpage.validity.valid
            && document.forms.formId.nickname.validity.valid
            && document.forms.formId.money.validity.valid);
}

const modified = document.lastModified;
document.getElementById('footer').innerHTML = modified;
