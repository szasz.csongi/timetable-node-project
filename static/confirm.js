// Allowing a teacher to log in
async function confirmUser(event, userId) {
    const update = await fetch(`/api/updateConfirmed/${userId}`);

    if (update.status === 200) {
        event.target.remove();
        const changeButton = document.getElementById(`userId${userId}`);
        changeButton.innerHTML = `${changeButton.innerHTML} <input class='btn btn-danger' onclick="blockUser(event, ${userId})" type='button' value='Block'> `;
    } else {
        const body = await update.json();
        const printError = document.getElementById(`userId=${userId}`);
        printError.innerHTML = `${printError.innerHTML} ${body.message}`;
    }
}

// Blocking a teacher
async function blockUser(event, userId) {
    const update = await fetch(`/api/updateNotConfirmed/${userId}`);

    if (update.status === 200) {
        event.target.remove();
        const changeButton = document.getElementById(`userId${userId}`);
        changeButton.innerHTML = `${changeButton.innerHTML} <input class='btn btn-success' onclick="confirmUser(event, ${userId})" type='button' value='Confirm'> `;
    } else {
        const body = await update.json();
        const printError = document.getElementById(`userId=${userId}`);
        printError.innerHTML = `${printError.innerHTML} ${body.message}`;
    }
}
