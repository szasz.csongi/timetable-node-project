// Show the pdfs
async function loadPdfs(subjectid) {
    const response = await fetch(`/api/getPdf/${subjectid}`);
    const body = await response.json();

    const idShowPdfs = document.getElementById(`showPdfs${subjectid}`);
    if (response.status === 200) {
        if (body.length === 0) {
            idShowPdfs.innerHTML = '';
            const div = document.createElement('div');
            div.textContent = 'No Pdfs';
            idShowPdfs.appendChild(div);
        } else {
            const div = document.createElement('div');
            idShowPdfs.innerHTML = '';
            div.innerHTML = 'Pdfs: <br>';
            idShowPdfs.appendChild(div);
            for (let i = 0; i < body.length; i += 1) {
                div.innerHTML = `${idShowPdfs.innerHTML} 
                <a href = "/${body[i].pdfName}" download>  
                    <b> DownloadPdfNr${body[i].pdfID}.pdf </b>  
                </a> `;
                idShowPdfs.appendChild(div);
            }
        }
    } else {
        const div = document.createElement('div');
        div.innerHTML = body.message;
        idShowPdfs.appendChild(div);
    }
}

// Delete the pdf and dissapear immediately
async function deletePdf(event, pdfID) {
    const response = await fetch(`/api/deletePdf/${pdfID}`, {
        method: 'DELETE',
    });

    if (response.status === 204) {
        const parent = event.target.parentNode.parentNode.parentNode;
        parent.remove();
        const alert = document.getElementById('showSuccesDelete');
        alert.style.display = 'block';
    } else {
        const body = await response.json();
        const printError = document.getElementById(`pdfID${pdfID}`);
        const div = document.createElement('div');
        div.innerHTML = `${printError.innerHTML} ${body.message}`;
        printError.appendChild(div);
    }
}
