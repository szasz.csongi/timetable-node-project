import express from 'express';
import { deleteRequest, insertTimetable } from '../db/acceptRequestDb.js';
import { selectTimetable, selectRequestedSubjects } from '../db/manageRequestsDb.js';
import { findSubjectById } from '../db/newSubjectDb.js';
import checkAcceptRequest from '../middleware/acceptRequestMw.js';
import { checkIfAdmin, checkJWTToken } from '../middleware/authMw.js';

const router = express.Router();

// Compare function for the sort algorithm
function compare(subject1, subject2) {
    if (subject1.day < subject2.day) {
        return -1;
    }
    if (subject1.day > subject2.day) {
        return 1;
    }
    if (subject1.hour < subject2.hour) {
        return -1;
    }
    if (subject1.hour > subject2.hour) {
        return 1;
    }
    return 0;
}

// GET for the accept request page
router.get('/acceptRequest', checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        const [timetableSubjects, requestedSubjects] = await Promise.all([selectTimetable(),
            selectRequestedSubjects(req.query.subjectId)]);
        const subjects = timetableSubjects.concat(requestedSubjects);
        subjects.sort(compare);
        const { subjectId } = req.query;
        res.render('accept_request', { subjects, subjectId });
    } catch (err) {
        res.write('Error in showing accept request page', err);
        res.status(500);
        res.end();
    }
});

// POST after acceptreques
router.post('/acceptRequest', checkJWTToken, checkIfAdmin, checkAcceptRequest, async (req, res) => {
    try {
        const subject = await findSubjectById(req.query.subjectId);
        const username = subject.teacherUsername;
        const data = {
            username,
            subjectid: req.query.subjectId,
            day: req.body.day,
            hour: req.body.hour,
        };

        await Promise.all([insertTimetable(data), deleteRequest(req.query.subjectId)]);
    } catch (err) {
        res.write('Error in inserting appointment to timetable', err);
        res.status(500);
        res.end();
    }
    res.redirect('/manageRequests/manageRequests');
});

// Decline the request
router.get('/declineRequest', checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        await deleteRequest(req.query.subjectId);
        res.redirect('/manageRequests/manageRequests');
    } catch (err) {
        res.write('Error in deleting request', err);
        res.status(500);
        res.end();
    }
});

export default router;
