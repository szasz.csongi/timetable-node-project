import express from 'express';
import eformidable from 'express-formidable';
import path from 'path';
import { existsSync, mkdirSync } from 'fs';

import { insertPdf, selectPdf } from '../db/subjectDeatilDb.js';
import { checkPdfUpload, checkSubjectIfExists, checkTeacherOwner } from '../middleware/subjectDetailMw.js';
import { findSubjectById } from '../db/newSubjectDb.js';

const router = express.Router();

const uploadDir = path.join(process.cwd(), 'uploadDir');
if (!existsSync(uploadDir)) {
    mkdirSync(uploadDir);
}

// Get for the subject detail page
router.get('/subject_detail', async (req, res) => {
    const id = req.query.subjectId;
    try {
        const [subject, pdfs] = await Promise.all([findSubjectById(id), selectPdf(id)]);
        res.render('subject_detail', { subject, pdfs });
    } catch (err) {
        res.write('Error in GET subject detail', err);
        res.status(500);
        res.end();
    }
});

router.use(eformidable({ uploadDir, keepExtensions: true }));

// Post a new pdf
router.post('/subjectDetail', checkPdfUpload, checkSubjectIfExists, checkTeacherOwner, async (req, res) => {
    const fileHandler = req.files.pdfForSubject;
    const generatedName = path.basename(fileHandler.path);

    const data = {
        subjectid: req.fields.subjectid,
        pdfName: generatedName,
    };

    try {
        await insertPdf(data);

        res.redirect('/');
    } catch (err) {
        res.write('Error in saving a pdf', err);
        res.status(500);
        res.end();
    }
});

export default router;
