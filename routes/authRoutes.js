import express from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import { checkRegistration, checkLogin } from '../middleware/authMw.js';
import { insertUser, selectUserByUsername } from '../db/authDb.js';
import secret from '../util/config.js';

const router = express.Router();

// GET for registration page
router.get('/registration', (req, res) => {
    res.render('registration');
});

router.post('/newRegistration', checkRegistration, async (req, res) => {
    const { password } = req.body;
    const { confirmPassword } = req.body;
    if (password !== confirmPassword) {
        return res.status(400).send('Password does not mach');
    }
    const passwordHash = await bcrypt.hash(password, 10);

    const data = {
        registrationFirstName: req.body.registrationFirstName,
        registrationLastName: req.body.registrationLastName,
        registrationUsername: req.body.registrationUsername,
        email: req.body.email,
        age: req.body.age,
        telephone: req.body.telephone,
        password: passwordHash,
    };

    try {
        await insertUser(data);
    } catch (err) {
        res.write('Registration error', err);
        res.status(500);
        res.end();
    }

    return res.redirect('/auth/login');
});

// GET for login page
router.get('/login', (req, res) => {
    res.render('login');
});

// Post after login
router.post('/newLogin', checkLogin, async (req, res) => {
    const username = req.body.loginUsername;
    const { password } = req.body;

    try {
        const stored = await selectUserByUsername(username);
        if (await bcrypt.compare(password, stored.password)) {
            const  { userRole } = stored;
            const token = jwt.sign({ username, userRole }, secret);

            res.cookie('token', token, {
                httpOnly: true,
                sameSite: 'strict',
            });

            return res.redirect('/');
        }
        return res.status(401).send('Incorrect login');
    } catch (err) {
        res.write('error', err);
        res.status(500);
        return res.end();
    }
});

// Logging out
router.get('/logout', (req, res) => {
    res.clearCookie('token');
    res.redirect('/');
});

export default router;
