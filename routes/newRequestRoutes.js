import express from 'express';
import { selectUserByUsername } from '../db/authDb.js';
import { selectUserRequestNumberByUsername, insertRequest, reduceRequestNumber } from '../db/newRequestDb.js';

import { findSubjectByTeacher } from '../db/subjectsDb.js';

import { checkJWTToken } from '../middleware/authMw.js';
import { checkRequest, checkNrRequests } from '../middleware/newRequestMw.js';

const router = express.Router();

// Get the new request page
router.get('/newRequest', checkJWTToken, async (req, res) => {
    try {
        const [subjects, requests] = await Promise.all(
            [findSubjectByTeacher(res.locals.payload.username),
                selectUserRequestNumberByUsername(res.locals.payload.username)],
        );

        const nrRequests = requests.requests;
        res.render('new_request', { subjects, nrRequests });
    } catch (err) {
        res.write('Error in showing the request page', err);
        res.status(500);
        res.end();
    }
});

// Post after a new request
router.post('/newRequest', checkJWTToken, checkRequest, checkNrRequests, async (req, res) => {
    const commands = [];
    for (let i = parseInt(req.body.hour, 10); i < parseInt(req.body.till, 10); i += 2) {
        const data = {
            username: res.locals.payload.username,
            subjectid: req.body.subjectid,
            day: parseInt(req.body.day, 10),
            hour: i,
            appropriate: req.body.appropriate,
        };
        commands.push(insertRequest(data));
    }

    try {
        await Promise.all(commands);
        const user = await selectUserByUsername(res.locals.payload.username);
        await reduceRequestNumber(user.userId);
    } catch (err) {
        res.write('Error in getting the user Id', err);
        res.status(500);
        return res.end();
    }
    return res.redirect('/');
});

export default router;
