import express from 'express';
import { findRequestedSubjects, selectTimetable } from '../db/manageRequestsDb.js';
import { checkIfAdmin, checkJWTToken } from '../middleware/authMw.js';

const router = express.Router();

// Get the manage request page
router.get('/manageRequests', checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        const [requests, subjects] = await Promise.all([findRequestedSubjects(),
            selectTimetable()]);
        res.render('manage_requests', { requests, subjects });
    } catch (err) {
        res.write('Error in showing the manage request page', err);
        res.status(500);
        res.end();
    }
});

export default router;
