import express from 'express';

import { findAllSubjects, findSubjectByTeacher } from '../db/subjectsDb.js';
import { checkJWTToken } from '../middleware/authMw.js';

const router = express.Router();

// GET for the main page
router.get('/', async (req, res) => {
    try {
        const subjects = await findAllSubjects();
        res.render('subjects', { subjects });
    } catch (err) {
        res.write('Error in showing the subjects', err);
        res.status(500);
        res.end();
    }
});

// Get after filtering just the logged in teacher subjects
router.get('/showJustLoggedInTeacherSubjects', checkJWTToken, async (req, res) => {
    try {
        const subjects = await findSubjectByTeacher(res.locals.payload.username);
        res.render('subjects', { subjects });
    } catch (err) {
        res.write('Error in showing the subjects', err);
        res.status(500);
        res.end();
    }
});

export default router;
