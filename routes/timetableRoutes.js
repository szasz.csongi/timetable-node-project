import express from 'express';
import { selectSubjectsFromTimetableByUsername, selectTimetable } from '../db/manageRequestsDb.js';
import { checkJWTToken } from '../middleware/authMw.js';

const router = express.Router();

// Get for the timetable page
router.get('/timetable', async (req, res) => {
    try {
        const subjects = await selectTimetable();
        res.render('timetable', { subjects });
    } catch (err) {
        res.write('Error in showing the timetable', err);
        res.status(500);
        res.end();
    }
});

// Get for just the logged in user timetable
router.get('/showJustLoggedInTeacherTimetable', checkJWTToken, async (req, res) => {
    try {
        const subjects = await selectSubjectsFromTimetableByUsername(res.locals.payload.username);
        res.render('timetable', { subjects });
    } catch (err) {
        res.write('Error in showing the timetable', err);
        res.status(500);
        res.end();
    }
});

export default router;
