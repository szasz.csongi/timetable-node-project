import express from 'express';

import { insertSubject } from '../db/newSubjectDb.js';
import checkSubject from '../middleware/newSubjectMw.js';

import { checkJWTToken } from '../middleware/authMw.js';

const router = express.Router();

// Get the new subject page
router.get('/new_subject', checkJWTToken, (req, res) => {
    res.render('new_subject');
});

// Post after creating anew page
router.post('/createSubject', checkSubject, checkJWTToken, async (req, res) => {
    try {
        await insertSubject(req.body, res.locals.payload.username);
        res.redirect('/');
    } catch (err) {
        res.write('Error in posting new subject', err);
        res.status(500);
        res.end();
    }
});

export default router;
