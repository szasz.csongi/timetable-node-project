import express from 'express';
import { selectPdf } from '../db/subjectDeatilDb.js';
import {
    deleteFromTimetableById, deletePdf, updateConfirmedUser, updateNotConfirmedUser,
} from '../db/apiDb.js';
import checkPdfsOwner from '../middleware/apiMw.js';
import { checkJWTToken, checkIfAdmin } from '../middleware/authMw.js';

const router = express.Router();

// GET the pdf
router.get('/getPdf/:id', async (req, res) => {
    try {
        const pdfs = await selectPdf(req.params.id);
        res.json(pdfs);
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
});

// delete the pdf
router.delete('/deletePdf/:id', checkPdfsOwner, async (req, res) => {
    try {
        await deletePdf(req.params.id);
        res.sendStatus(204);
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
});

// delete row from timetable
router.delete('/deleteTimetable/:id', checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        await deleteFromTimetableById(req.params.id);
        res.sendStatus(204);
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
});

// Allowing a user to log in
router.get('/updateConfirmed/:id', checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        await updateConfirmedUser(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
});

// Blocking a user
router.get('/updateNotConfirmed/:id', checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        await updateNotConfirmedUser(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        res.status(500).send({ message: err.message });
    }
});

export default router;
