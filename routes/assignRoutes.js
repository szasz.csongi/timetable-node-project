import express from 'express';

import { checkIds, checkSubjectIfExists, checkUserIfExists } from '../middleware/assignMw.js';
import { insertAttendence, deleteAttendence, searchBySubjectIdAndStudentId } from '../db/assignDb.js';
import { findAllSubjects } from '../db/subjectsDb.js';
import { findAllUsers } from '../db/usersDb.js';

const router = express.Router();

router.get('/assign', async (req, res) => {
    try {
        const [subjects, users] = await Promise.all([findAllSubjects(), findAllUsers()]);
        res.render('assign', { subjects, users });
    } catch (err) {
        res.write('Error in showing the assign page', err);
        res.status(500);
        res.end();
    }
});

router.post('/studentJoinOrLeave', checkIds, checkSubjectIfExists, checkUserIfExists, async (req, res) => {
    try {
        const [exists, subjects, users] = await Promise.all(
            [searchBySubjectIdAndStudentId(req.body), findAllSubjects(), findAllUsers()],
        );
        if (req.body.joinOrLeave === 'join') {
            if (typeof exists === 'undefined') {
                try {
                    await insertAttendence(req.body);
                    const succes = `You have succesfully joined to this subject: ${req.body.subjectid}`;
                    return res.render('assign', { subjects, users, succes });
                } catch (err) {
                    res.write('Error in posting new attendance', err);
                    res.status(500);
                    return res.end();
                }
            }
            const error = `You are allready assigned to this subject: ${req.body.subjectid}`;
            return res.render('assign', { subjects, users, error });
        }
        if (typeof exists !== 'undefined') {
            try {
                await deleteAttendence(req.body);
                const succes = `You have succesfully left from this subject: ${req.body.subjectid}`;
                return res.render('assign', { subjects, users, succes });
            } catch (err) {
                res.write('Error in posting new attendance', err);
                res.status(500);
                return res.end();
            }
        }
        const error = `You were not joined to this subject: ${req.body.subjectid}`;
        return res.render('assign', { subjects, users, error });
    } catch (err) {
        res.write('Error in finding attendance', err);
        res.status(500);
        return res.end();
    }
});

export default router;
