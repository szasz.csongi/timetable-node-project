import express from 'express';
// import * as db from '../db/usersDb.js';
import { findAllUsers } from '../db/usersDb.js';
import { checkJWTToken, checkIfAdmin } from '../middleware/authMw.js';

const router = express.Router();

// GET for list of users page
router.get('/users', checkJWTToken, checkIfAdmin, async (req, res) => {
    try {
        const users = await findAllUsers();
        res.render('users', { users });
    } catch (err) {
        res.write('Users GET error', err);
        res.status(500);
        res.end();
    }
});

export default router;
