import express from 'express';
import { deleteRequest, deleteTimetableBySubjectId } from '../db/acceptRequestDb.js';
import { updateSubject, deleteSubject, deletePdfsOfASubject } from '../db/editSubjectDb.js';

import { findSubjectById } from '../db/newSubjectDb.js';
import { findTeachers } from '../db/usersDb.js';
import { checkJWTToken } from '../middleware/authMw.js';
import { checkEditSubject, checkTeacherOwnerOrAdmin } from '../middleware/editSubjectMw.js';

const router = express.Router();

// GET edit subject page
router.get('/getEditSubject', checkJWTToken, checkTeacherOwnerOrAdmin, async (req, res) => {
    const id = req.query.subjectId;
    try {
        const [subject, teachers] = await Promise.all([findSubjectById(id), findTeachers()]);
        res.render('edit_subject', { subject, teachers });
    }  catch (err) {
        res.write('Error in GET subject edit', err);
        res.status(500);
        res.end();
    }
});

// Delete a subject
router.get('/deleteSubject', checkJWTToken, checkTeacherOwnerOrAdmin, async (req, res) => {
    const id = req.query.subjectId;
    try {
        await Promise.all([deleteSubject(id), deletePdfsOfASubject(id),
            deleteRequest(id), deleteTimetableBySubjectId(id)]);
        res.redirect('/');
    }  catch (err) {
        res.write('Error in deleting subject', err);
        res.status(500);
        res.end();
    }
});

// Post after editing the subject
router.post('/editSubject', checkJWTToken, checkTeacherOwnerOrAdmin, checkEditSubject, async (req, res) => {
    if (typeof req.body.teacherUsername === 'undefined') {
        return res.status(400).send('Subject owner is missing');
    }

    if (res.locals.payload.userRole !== 1) {
        req.body.teacherUsername = res.locals.payload.username;
    }

    const id = req.query.subjectId;

    try {
        await updateSubject(req.body, id);
        return res.redirect('/');
    } catch (err) {
        res.write('Error in editing subject', err);
        res.status(500);
        return res.end();
    }
});

export default router;
