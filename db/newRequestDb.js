import callQuery from './connection.js';

// Find users info by username
export function selectUserRequestNumberByUsername(username) {
    const queryStr = 'SELECT users.requests FROM users WHERE users.username = ?';
    return callQuery(queryStr, [username]).then((user) => user[0]);
}

// Insert a new request to the datbase
export function insertRequest(data) {
    const queryStr = `INSERT INTO requests
    VALUES (default, ?, ?, ?, ?, ?)`;
    return callQuery(queryStr, [data.username,
        data.subjectid, data.day, data.hour, data.appropriate]);
}

// Reduce the number of the requests of a user by his id
export function reduceRequestNumber(id) {
    const queryStr = `UPDATE users 
    SET users.requests = users.requests - 1 
    WHERE users.userId = ?`;
    return callQuery(queryStr, [id]);
}
