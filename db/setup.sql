CREATE DATABASE IF NOT EXISTS timetable;

USE timetable;
GRANT ALL PRIVILEGES ON *.* TO 'timetable'@'localhost' IDENTIFIED BY 'Csongi123csongi';


ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Csongi123csongi';
flush privileges;

-- change root password for Maria-DB
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('Csongi123csongi');
flush privileges;