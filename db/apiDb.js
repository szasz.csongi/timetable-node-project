import callQuery from './connection.js';

// delete pdf
export function deletePdf(id) {
    const queryStr = 'DELETE FROM pdfs WHERE pdfs.pdfID = ?';
    return callQuery(queryStr, [id]);
}

// delete a subject from timetable by timetableId
export function deleteFromTimetableById(id) {
    const queryStr = 'DELETE FROM timetable WHERE timetable.timetableId = ?';
    return callQuery(queryStr, [id]);
}

// Get the owner of a pdf
export function findOwnerOfPdf(pdfId) {
    const queryStr = `SELECT subjects.teacherUsername FROM subjects
    JOIN pdfs ON pdfs.subjectid = subjects.subjectid
    WHERE pdfs.pdfID = ?`;
    return callQuery(queryStr, [pdfId]).then((username) => username[0]);
}

// Allowing a user to be able to log in
export function updateConfirmedUser(userId) {
    const queryStr = `UPDATE users 
    SET users.confirmed = 1 
    WHERE users.userId = ?`;
    return callQuery(queryStr, [userId]);
}

// Blocking a user
export function updateNotConfirmedUser(userId) {
    const queryStr = `UPDATE users 
    SET users.confirmed = 0 
    WHERE users.userId = ?`;
    return callQuery(queryStr, [userId]);
}
