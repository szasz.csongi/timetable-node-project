import callQuery from './connection.js';

// Find the requestions
export function findRequestedSubjects() {
    return callQuery('SELECT requests.subjectid FROM requests GROUP BY requests.subjectid');
}

// Find the subjects inserted to the timetable
export function selectTimetable() {
    return callQuery('SELECT * FROM timetable ORDER BY timetable.day, timetable.hour');
}

// Find the subjects inserted to the timetable by owner username
export function selectSubjectsFromTimetableByUsername(username) {
    const queryStr = 'SELECT * FROM timetable WHERE timetable.username = ? ORDER BY timetable.day, timetable.hour';
    return callQuery(queryStr, [username]);
}

// Select from requests by subjectid
export function selectRequestedSubjects(subjectid) {
    const queryStr = 'SELECT * FROM requests WHERE requests.subjectid = ? ORDER BY requests.day, requests.hour';
    return callQuery(queryStr, [subjectid]);
}
