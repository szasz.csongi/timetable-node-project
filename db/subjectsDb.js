import callQuery from './connection.js';

// Find all subjects and its info
export function findAllSubjects() {
    return callQuery('SELECT * FROM subjects');
}

// Find a teachers subjects
export function findSubjectByTeacher(username) {
    const queryStr = 'SELECT * FROM subjects WHERE subjects.teacherUsername = ?';
    return callQuery(queryStr, [username]);
}

// Find a subject by its id
export function findSubjectsById(id) {
    const queryStr = 'SELECT * FROM subjects WHERE subjects.subjectid = ?';
    return callQuery(queryStr, [id]).then((fields) => fields[0]);
}
