import callQuery from './connection.js';

// isnert a new subject to the timetable
export function insertTimetable(data) {
    const queryStr = `INSERT INTO timetable
    VALUES (default, ?, ?, ?, ?, 2)`;
    return callQuery(queryStr, [data.username, data.subjectid,
        data.day, data.hour]);
}

// delete a request from the request table
export function deleteRequest(subjectid) {
    const queryStr = 'DELETE FROM requests WHERE requests.subjectid = ?';
    return callQuery(queryStr, [subjectid]);
}

// delete a subject from the timetable by subject id
export function deleteTimetableBySubjectId(subjectid) {
    const queryStr = 'DELETE FROM timetable WHERE timetable.subjectid = ?';
    return callQuery(queryStr, [subjectid]);
}
