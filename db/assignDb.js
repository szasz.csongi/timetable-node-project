import callQuery from './connection.js';

export function insertAttendence(data) {
    const queryStr = `INSERT INTO attendance
    VALUES (default, ?, ?)`;
    return callQuery(queryStr, [data.subjectid, data.studentid]);
}

export function deleteAttendence(data) {
    const queryStr = `DELETE FROM attendance
    WHERE attendance.subjectid = ?
    AND attendance.studentid = ?`;
    return callQuery(queryStr, [data.subjectid, data.studentid]);
}

export function searchBySubjectIdAndStudentId(data) {
    const queryStr = `SELECT * FROM attendance
    WHERE attendance.subjectid = ?
    AND attendance.studentid = ?`;
    return callQuery(queryStr, [data.subjectid, data.studentid]).then((fields) => fields[0]);
}
