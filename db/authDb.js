import callQuery from './connection.js';

// Check if this username is allready used or not
export function checkIfExistsUser(username) {
    const queryStr = `SELECT COUNT(*) as Counter
    FROM users
    WHERE users.username = ?`;
    return callQuery(queryStr, [username]).then((users) => users[0]);
}

// Inserting a new user into database after registration
export function insertUser(data) {
    const queryStr = `INSERT INTO users
    VALUES (default, ?, ?, ?, ?, ?, ?, ?, 0, 0, 3)`;
    return callQuery(queryStr, [data.registrationFirstName, data.registrationLastName,
        data.registrationUsername, data.email, data.age, data.telephone, data.password]);
}

// Find the informations of a user by username
export function selectUserByUsername(username) {
    const queryStr = 'SELECT * FROM users WHERE users.username = ?';
    return callQuery(queryStr, [username]).then((user) => user[0]);
}
