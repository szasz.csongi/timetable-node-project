import mysql from 'mysql';
import util from 'util';

const connectionPool = mysql.createPool({
    connectionLimit: 10,
    database: 'timetable',
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'Csongi123csongi',
});

const callQuery = util.promisify(connectionPool.query).bind(connectionPool);

export default callQuery;

// Create subjects table
function createSubject() {
    return callQuery(`CREATE TABLE IF NOT EXISTS subjects (
        subjectid VARCHAR(255) PRIMARY KEY,
        subjectname VARCHAR(255),
        grade INT,
        course INT,
        seminar INT,
        labor INT,
        teacherUsername VARCHAR(255)
    )`);
}

// Create pdfs table
function createPdfDatabase() {
    return callQuery(`CREATE TABLE IF NOT EXISTS pdfs (
        pdfID INT PRIMARY KEY AUTO_INCREMENT,
        subjectid VARCHAR(255),
        pdfName VARCHAR(255)
    )`);
}

function createStudentSubject() {
    return callQuery(`CREATE TABLE IF NOT EXISTS attendance (
        attendanceId INT PRIMARY KEY AUTO_INCREMENT,
        subjectid VARCHAR(255),
        studentid VARCHAR(255)
    )`);
}

// Create users table
function createUsers() {
    return callQuery(`CREATE TABLE IF NOT EXISTS users (
        userId INT PRIMARY KEY AUTO_INCREMENT,
        firstName VARCHAR(255),
        lastName VARCHAR(255),
        username VARCHAR(255),
        email VARCHAR(255),
        age INT,
        telephone VARCHAR(255),
        password VARCHAR(255),
        userRole INT,
        confirmed INT,
        requests INT
    )`);
}

// Create requests table
function createRequests() {
    return callQuery(`CREATE TABLE IF NOT EXISTS requests (
        requestId INT PRIMARY KEY AUTO_INCREMENT,
        username VARCHAR(255),
        subjectid VARCHAR(255),
        day INT,
        hour INT,
        appropriate INT
    )`);
}

// Create timetable table
function createTimetable() {
    return callQuery(`CREATE TABLE IF NOT EXISTS timetable (
        timetableId INT PRIMARY KEY AUTO_INCREMENT,
        username VARCHAR(255),
        subjectid VARCHAR(255),
        day INT,
        hour INT,
        appropriate INT
    )`);
}

(async () => {
    try {
        await createUsers();
        await createSubject();
        await createPdfDatabase();
        await createStudentSubject();
        await createRequests();
        await createTimetable();
    } catch (err) {
        console.error(err);
    }
})();
