import callQuery from './connection.js';

// Find all users
export function findAllUsers() {
    return callQuery('SELECT * FROM users');
}

// Find the teachers
export function findTeachers() {
    return callQuery('SELECT users.username FROM users WHERE users.confirmed=1');
}
