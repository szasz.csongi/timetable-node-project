import callQuery from './connection.js';

// Insert a new subject to the database
export function insertSubject(data, username) {
    const queryStr = `INSERT INTO subjects
    VALUES (?, ?, ?, ?, ?, ?, ?)`;
    return callQuery(queryStr, [data.subjectid, data.subjectname,
        data.grade, data.course, data.seminar, data.labor, username]);
}

// Find the subjects info by its id
export function findSubjectById(data) {
    const queryStr = `SELECT * FROM subjects
    WHERE subjects.subjectid = ?`;
    return callQuery(queryStr, [data]).then((fields) => fields[0]);
}

// Find a user by his id
export function findUserById(data) {
    const queryStr = `SELECT * FROM users
    WHERE users.userId = ?`;
    return callQuery(queryStr, [data]).then((fields) => fields[0]);
}
