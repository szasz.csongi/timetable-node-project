import callQuery from './connection.js';

// insert a new pdf to the database
export function insertPdf(data) {
    const queryStr = `INSERT INTO Pdfs 
    VALUES (default, ?, ?)`;
    return callQuery(queryStr, [data.subjectid, data.pdfName]);
}

// select a pdf by its id
export function selectPdf(id) {
    const queryStr = 'SELECT * FROM pdfs WHERE pdfs.subjectid = ?';
    return callQuery(queryStr, [id]);
}
