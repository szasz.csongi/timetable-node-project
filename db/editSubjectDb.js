import callQuery from './connection.js';

// Modifyin the data of a subject
export function updateSubject(data, id) {
    const queryStr = `UPDATE subjects
    SET subjects.subjectid = ?, subjects.subjectname = ?,
    subjects.grade = ?, subjects.course = ?, 
    subjects.seminar = ?, subjects.labor = ?,
    subjects.teacherUsername = ?
    WHERE subjects.subjectid = ?`;
    return callQuery(queryStr, [data.subjectid, data.subjectname,
        data.grade, data.course, data.seminar, data.labor, data.teacherUsername, id]);
}

// Delete subject from database
export function deleteSubject(id) {
    const queryStr = 'DELETE FROM subjects WHERE subjects.subjectid = ?';
    return callQuery(queryStr, [id]);
}

// Delete the pdfs of a subject
export function deletePdfsOfASubject(id) {
    const queryStr = 'DELETE FROM pdfs WHERE pdfs.subjectid = ?';
    return callQuery(queryStr, [id]);
}
